/*
Mark Giebler 2017-01-08
  Shift Register Example
 for MC145000 LCD shift register LCD controller

 This sketch turns reads serial input and uses it to set the pins
 of a MC145000 shift register.
 It also displays a 4 digit count on the LCD.

 Hardware:
 * MC145000 shift register attached to pins 12, 11, and 10 of the Arduino,
 * LCD segments attached to each of the outputs of the shift register
 * 
Bit 0 -8 are for indicators: C, F, etc....
Bit 9 is first segment of right most digit.
Bit 39 is last segment of digits.  There are some unused bits in the range.
See mc145000.cpp for details
 */


// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// 			MC145000  LCD module Driver
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
#include "mc145000.h"

// =================================================================
// -----------------------------------------------------------------
//  				app code below
// -----------------------------------------------------------------
// =================================================================


//int led = 21; // fubarino SD  LED on Pin 21.
int led = 13;	// std Arduino LED pin.
unsigned long count=0;
void LCDsetup(int clk, int data, int sync);
 
void setup() {
	LCDsetup(12,11,10); 		// set pins: clock, data, sync pin #
	pinMode(led, OUTPUT); 
	digitalWrite(led, HIGH);	// turn the LED on 
	Serial.begin(57600);

	delay(3000);
	Serial.println("reset");
}
int blink=1;
void loop() {
	if (Serial.available() > 0) {
		// ASCII '0' through '9' to binary
		int bitToSet = Serial.read() - 48;

		// set LCD segment register with the requested bit set high:
		setLCDsegment(bitToSet, HIGH);
		Serial.println("Input bit 0 to 7:");
	}
	blink ^= 1;
	digitalWrite(led, blink);
	Serial.print("Count: ");
	/*
	// Hex digit test
	Serial.print(count,HEX);
	LCDarray[1]=LCD_HexDigits[count];
	Serial.print("  Segments: ");
	Serial.print(LCDarray[1],BIN);

	updateLCD(LCDarray);
	Serial.println();
	delay(2200);
	++count;
	count %= 16;	// 0 to F
	
	*/

	// LCDprint method test
	Serial.println(count,DEC);
	LCDprint(count);
	delay(400);
	++count;
	count %= 10000;	// range 0 to 9999 
}

void setLCDsegment(int whichBit, int whichState) {
	// the bits you want to send
	byte bitsToSend = 0;

	// turn on the requested bit in bitsToSend:
	bitWrite(bitsToSend, whichBit, whichState);

	// shift the bits out:
	LCDsetDigit(2, bitsToSend);
	LCDupdate();
}


