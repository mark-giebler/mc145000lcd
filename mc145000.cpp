// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// 			MC145000  LCD module Driver
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// Mark Giebler
#include "mc145000.h"
#include <Arduino.h>

// set default pin mapping, over written by LCDsetup()
//Pin connected to clock pin (SH_CP) of MC145000
int clockPin = 12;
//Pin connected to Data in (DI) of MC145000
int dataPin = 11;
// Pin connected to the Sync output of MC145000
int syncPin = 10;

// prototypes for my MC145000 LCD driver.
void LCDsetup(int clk, int data, int sync);
void LCDprint(int number);
void LCDsetDigit(uint8_t index, uint8_t bits);
void LCDupdate();

const unsigned char LCD_HexDigits[] ={
  LCD_0,LCD_1,LCD_2,LCD_3,LCD_4,LCD_5,LCD_6,LCD_7,LCD_8,LCD_9,LCD_A,LCD_B,LCD_C,LCD_D,LCD_E,LCD_F
 };
 // LCDarray  index 0 is right most LCD digit.
unsigned char LCDarray[]={LCD_BLANK,LCD_0,LCD_BLANK,LCD_BLANK,LCD_BLANK,LCD_BLANK,0};

void LCDwrite(unsigned char* digits);
void LCDupdate();

// LCDsetup()
// Setup the pin numbers and modes for controlling the LCD
void LCDsetup(int clk, int data, int sync)
{
	// set pin number mapping
	clockPin=clk;
	dataPin=data;
	syncPin=sync;

	digitalWrite(dataPin, LOW);
	digitalWrite(clockPin, LOW);

	pinMode(dataPin, OUTPUT);  
	pinMode(clockPin, OUTPUT);
	pinMode(syncPin, INPUT);
	LCDwrite(LCDarray);	// set the default data to the display
}

// LCDwrite()
// This method sends LCD segment bits to the shift register:
// digits[0] is right most digit.
void LCDwrite(unsigned char* digits)
{
	for(int i=0;i<6;i++)
	{
		// shift 8 bits out:
		shiftOut(dataPin, clockPin, MSBFIRST, digits[5-i]);
	}
}
// LCDupdate()
// Write the data in the LCDarray bit pattern to the shift register
void LCDupdate()
{
	LCDwrite(LCDarray);
}
// LCDprint()
// put a decimal number on the LCD  (digit positions 1 to 4)
//	input: range 0 - 9999
void LCDprint(int number)
{
	// output 4 digits to the LCD.  LCDarray[0] is right most position on LCD.
	int value = number;
	int digit = 0;
	int div = 1000;
	int i = 5;
	int leadingBlank = 1;
	// enforce positive only
	if(number < 0 )
		value *= -1;
	// enforce range 0 - 9999 by clipping upper digits off.
	value %= 10000;

	i--;
	while(i > 0){
		digit = value/div;
    
		if(leadingBlank && digit == 0 && i != 1 ){
			LCDarray[i--] = LCD_BLANK;
		}else{	
			LCDarray[i--] = LCD_HexDigits[digit];
			leadingBlank=0;  // no more zero blanking.
		}
		value %= div;
		div /= 10;
	}
	LCDwrite(LCDarray);
}
// LCDsetDigit()
// Set bits in the LCDarray[] buffer (does not update actual display)
//	use LCDupdate() to update actual display.
// index - which digit in the LCD array to set: 0-5  (Right to left)
//			0 and 5 are indicators, 1-4 are 7 segment digits.
// bits - bits to set. (control individual segments)
//  use mainly to control the non digit icons.( index 0 and 5)
void LCDsetDigit(uint8_t index, uint8_t bits)
{
	if(index >= 0 && index <= 5)
		LCDarray[index]=bits;
}

